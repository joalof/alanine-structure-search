import io
from contextlib import redirect_stdout

import numpy as np
import GPy
import py3Dmol
from ipywidgets import interact, interactive, fixed, IntSlider, Layout

from code import gcutil


class Alanine():

    def __init__(self, dim):

        if dim not in [2, 4]:
            raise ValueError(f'Alanine example is not available in dimension {dim}.')

        self.model = load_model(f'models/model_{dim}D_E0.npz')

    @property
    def dim(self):
        return self.model.input_dim

    @property
    def bounds(self):
        return [[0, self.model.std_periodic.period[d]] for d in range(self.dim)]

    def calculate_energy(self, x):
        return self.model.predict(np.atleast_2d(x))[0]

    def convert(self, x):
        # load base structure
        atomnames, rconnect, r, aconnect, a, dconnect, d = gcutil.readzmat('models/alanine.gzmat')

        # update rotations
        if self.dim == 2:
            d[0] = x[0]  # d4
            d[1] = x[0] + 120  # d5
            d[9] = x[1]  # d13
        if self.dim == 4:
            d[0] = x[0]  # d4
            d[1] = x[0] + 120  # d5
            d[3] = x[1]  # d7
            d[4] = x[1] + 120  # d8
            d[5] = x[1] + 240  # d9
            d[7] = x[2]  # d11
            d[8] = x[2] + 180  # d12
            d[9] = x[3]  # d13

        # convert to xyz
        f = io.StringIO()
        with redirect_stdout(f):
            gcutil.write_xyz(atomnames, rconnect, r, aconnect, a, dconnect, d)
        xyz_str = f.getvalue()

        return xyz_str

    def view_structure(self, x):
        xyz_str = self.convert(np.squeeze(x))
        view = py3Dmol.view(width=500, height=500)
        view.addModel(xyz_str, "xyz")
        view.setStyle({'stick': {}, 'sphere': {'scale': .35}}, viewer=(0, 0))
        view.setBackgroundColor('0xeeeeee', viewer=(0, 0))
        view.setView([ -0, -0, -0, 125, -0.15, -0.5, 0, -0.85])
        view.show()

    def view_all(self, res):

        def drawit(m, p, iteration=0):
            x = res.select('X', iteration)[0]
            m = self.convert(np.squeeze(x))
            p.removeAllModels()
            p.addModel(m, "xyz")
            p.setStyle({'stick': {}, 'sphere': {'scale': .35}}, viewer=(0, 0))
            p.setBackgroundColor('0xeeeeee', viewer=(0, 0))
            p.setView([ -0, -0, -0, 125, -0.15, -0.5, 0, -0.85])  # TODO
            p.zoomTo()
            return p.show()

        iterpts = res.settings['iterpts']
        x = res.select('X', 0)[0]
        m = self.convert(np.squeeze(x))
        p = py3Dmol.view(width=500, height=500)
        iteration_slider = IntSlider(1, 1, 40, 1, layout=Layout(width='500px'))
        interact(drawit, m=fixed(m), p=fixed(p), iteration=iteration_slider)

def load_model(filename):
    # load saved data
    saved = np.load(filename)
    dim = saved['X'].shape[1]

    # create kernel
    k = GPy.kern.StdPeriodic(input_dim=dim, ARD1=True, ARD2=True)

    # create mean function
    mf = GPy.mappings.Constant(dim, 1)

    # create model
    model = GPy.models.GPRegression(saved['X'], saved['Y'], kernel=k, mean_function=mf)

    # set model params
    model[:] = saved['params']
    model.fix()
    model.parameters_changed()

    return model
